from django.shortcuts import render
from SPARQLWrapper import SPARQLWrapper, JSON
import rdflib

# Create your views here.
def index(request):
    if request.method == "POST":
        input = {}
        input['title'] = request.POST.get('title')
        input['category'] = request.POST.get('category')
        
        local_input = build_local_filter(input)
        local_res = local_query(local_input)
        
        input = build_filter(input)
        res = dbpedia_query(input)
        
        # if res == "SPARQL Error!" or local_res == "SPARQL Error!":
        #     print('masuk')
        #     return render(request, 'home.html')

        if local_res == "SPARQL Error!":
            print("local res error!")
            return render(request, 'home.html')

        elif  res == "SPARQL Error!":
            print('res error!')
            return render(request, 'home.html')

        # if local_res == "SPARQL Error!":
        #     print("local res error!")
        #     return render(request, 'home.html')

        else:
            res = wikidata_query(res)
            res = combine(res, local_res)
            res = preprocessing(res)
            #TODO: process local query result (combine local_query + dbpedia query)
            return render(request, 'home.html', {'results': res.values()})
        
    return render(request, 'home.html')

def local_query(input):
    query = """
        SELECT ?film ?title ?date_added ?release_year ?duration ?description 
        (group_concat(?rating;separator=", ") as ?ratings) 
        (group_concat(?country;separator=", ") as ?countries) 
        (group_concat(?director;separator=", ") as ?directors)
        (group_concat(?directorName;separator=", ") as ?directorsName) 
        (group_concat(?cast;separator=", ") as ?casts) 
        (group_concat(?castName;separator=", ") as ?castsName)
        (group_concat(?genre;separator=", ") as ?genres) 
        WHERE 
        {{
            {}
                dbp:title ?title ;
                dbp:released ?release_year ;
                ns1:runtime ?duration ;
                dbo:abstract ?description .
                
            OPTIONAL {{?film ex:date_added ?date_added}}
            OPTIONAL {{?film ex:rating ?rating}}
            OPTIONAL {{?film dbp:country ?country}}
            OPTIONAL {{ ?film dbo:director ?director .
                            ?director dbp:Name ?directorName .}}      
            OPTIONAL {{?film dbo:starring ?cast .
                            ?cast dbp:Name ?castName .}} 
            OPTIONAL {{?film dbp:genre ?genre}} 
                

            FILTER regex(lcase(?title), '{}')
            
        }}
        GROUP BY ?film ?title ?date_added ?release_year ?duration ?description
        """.format('?film a dbo:{} ;'.format(input[0]), input[1])
    cast_query = """
        SELECT ?film ?title ?date_added ?release_year ?duration ?description 
        (group_concat(?rating;separator=", ") as ?ratings) 
        (group_concat(?country;separator=", ") as ?countries) 
        (group_concat(?director;separator=", ") as ?directors)
        (group_concat(?directorName;separator=", ") as ?directorsName) 
        (group_concat(?cast;separator=", ") as ?casts) 
        (group_concat(?castName;separator=", ") as ?castsName)
        (group_concat(?genre;separator=", ") as ?genres) 
        WHERE 
        {{
            {{
                SELECT ?film WHERE {{
                    {{ ?film a dbo:Film . }} UNION {{ ?film a dbo:TelevisionShow . }}
                    ?film dbo:starring ?c .
                    ?c dbp:Name ?cn
                    filter (regex(lcase(?cn), '{}'))
                }}   
            }}    
            ?film dbp:title ?title .
            ?film dbp:released ?release_year .
            ?film ns1:runtime ?duration .
            ?film dbo:abstract ?description . 
            OPTIONAL {{?film ex:date_added ?date_added}}
            OPTIONAL {{?film ex:rating ?rating}}
            OPTIONAL {{?film dbp:country ?country}}
            OPTIONAL {{ ?film dbo:director ?director .
                            ?director dbp:Name ?directorName .}}      
            OPTIONAL {{?film dbo:starring ?cast .
                            ?cast dbp:Name ?castName .}} 
            OPTIONAL {{?film dbp:genre ?genre}} 
            
        }}
        GROUP BY ?film ?title ?date_added ?release_year ?duration ?description
    """.format(input[1])
    g = rdflib.Graph()
    g.parse('static/output.ttl')
    
    try:
        if input[0] == 'Cast':
            res = g.query(cast_query)
            res = process_local_result(res)
            return res
        else:
            res = g.query(query)
            res = process_local_result(res)
            return res
    
    except Exception:
        return "SPARQL Error!"
    
    
def dbpedia_query(input):
    query = """
        SELECT ?title ?wiki (group_concat(distinct ?cast;separator=", ") as ?casts) (group_concat(distinct ?castName;separator=", ") as ?castsName) (group_concat(distinct ?director;separator=", ") as ?directors) (group_concat(distinct ?directorName;separator=", ") as ?directorsName)  (group_concat(distinct ?producer;separator=", ") as ?producers) (group_concat(distinct ?producerName;separator=", ") as ?producersName) (group_concat(distinct ?writer;separator=", ") as ?writers)  (group_concat(distinct ?writerName;separator=", ") as ?writersName) (group_concat(distinct ?genre;separator=", ") as ?genres) ?country ?runtime ?desc ?release WHERE
            {{
                ?film rdfs:label ?title ;
                    dbo:starring ?cast ;
                    rdf:type ?category ;
                    dbp:country ?country ;
                    dbo:Work\/runtime ?runtime ;
                    dbo:abstract ?desc ;
                    owl:sameAs ?wiki ;
                    dbo:distributor [ dbo:parentCompany ?distributor ] .
                ?cast rdfs:label ?castName .
                {}
                OPTIONAL {{ ?film dbo:director ?director .
                                    ?director foaf:name ?directorName .
                                    FILTER (lang(?directorName) = "en") }}
                OPTIONAL {{ ?film dbo:producer ?producer .
                                    ?producer foaf:name ?producerName .
                                    FILTER (lang(?producerName) = "en") }}
                OPTIONAL {{ ?film dbo:writer ?writer.
                                    ?writer foaf:name ?writerName .
                                    FILTER (lang(?writerName) = "en") }}
                OPTIONAL {{ ?film dbp:released ?release }}
                OPTIONAL {{ ?film dbp:genre ?genre }}
                FILTER (regex(str(?wiki), "wikidata") && lang(?title) = "en" && lang(?desc) = "en" && lang(?castName) = "en" && regex(str(?distributor), "Disney") {})
            }}
        GROUP BY ?title ?wiki ?country ?runtime ?desc ?release 

    """.format('?film a dbo:Film' if input[0] == 'film' else '?film a dbo:TelevisionShow' if input[0] == 'tv show' else subquery(input), input[1] if input[0] == 'film' or input[0] == 'tv show' else '')

    # print(query)

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)

    try:
        res = sparql.query().convert()
        for movie in res['results']['bindings']:
            # print(movie)
            break
        return res
    except Exception:
        return "SPARQL Error!"

def subquery(input):
    return """
    {{
      SELECT ?film WHERE 
      {{
          {{ ?film a dbo:Film . }} UNION {{ ?film a dbo:TelevisionShow . }}
          ?film dbo:starring ?c .
          ?c rdfs:label ?cn .
          FILTER ( regex(lcase(?cn), \'{}\') )
      }}  
    }}
    """.format(str(input[1]))

def wikidata_query(input):
    for movie in input['results']['bindings']:
        query_wiki = """
        SELECT (group_concat(distinct ?review; separator=", ") as ?reviews) (group_concat(distinct ?genre; separator=", ") as ?genres)
        WHERE 
        {{
            {} rdfs:label ?itemLabel ;
                        wdt:P136 [ rdfs:label ?genre ] .
            OPTIONAL {{ {} wdt:P444 ?review .
                        FILTER (REGEX(STR(?review), "/10")) }}
            FILTER( LANG(?itemLabel) = "en" && LANG(?genre) = 'en' ) 
        }}
        GROUP BY ?itemLabel ?review
        """.format('wd:'+movie['wiki']['value'][31:], 'wd:'+movie['wiki']['value'][31:])

        sparql = SPARQLWrapper("https://query.wikidata.org/sparql")

        sparql.setQuery(query_wiki)
        sparql.setReturnFormat(JSON)

        try:
            ret_wiki = sparql.query().convert()
            # pd = json_normalize(ret["results"]["bindings"])
            # print(ret_wiki['results']['bindings'])
            
            if ret_wiki['results']['bindings'] == [] or ret_wiki['results']['bindings'][0]['reviews']['value'] == '':
                movie['review_score'] = '-'
            else:
                movie['review_score'] = ret_wiki['results']['bindings'][0]['reviews']['value']

            if ret_wiki['results']['bindings'] == [] or ret_wiki['results']['bindings'][0]['genres']['value'] == '':
                movie['genres'] = ""
            else:
                movie['genres'] = ret_wiki['results']['bindings'][0]['genres']['value']
    
        except Exception:
            return "SPARQL Error!"
        
    return input
        

def build_filter(input):
    filters = []
    title = input['title']
    category = input['category']

    if category == 'Film':
        filters.append('film')
        filters.append('&& (regex(lcase(?title), "{}" )) '.format(title.lower()))
    elif category == 'TV Show':
        filters.append('tv show')
        filters.append('&& (regex(lcase(?title), "{}" )) '.format(title.lower()))
    elif category == 'Cast':
        filters.append('cast')
        filters.append(title.lower())
    
    return filters

def build_local_filter(input):
    filters = []
    title = input['title']
    category = input['category']

    if category == 'Film':
        filters.append('Film')
    elif category == 'TV Show':
        filters.append('TelevisionShow')
    elif category == 'Cast':
        filters.append('Cast')
    filters.append(title.lower())
    
    return filters

def process_local_result(res):
    res_dicts = {}

    for row in res:
        res_dict = {}
        directors = set()
        directors_name = set()
        casts = set()
        casts_name = set()
        countries = set()
        ratings = set()
        genres = set()

        for director in row.directors.toPython().split(", "):
            directors.add(director)
        for director_name in row.directorsName.toPython().split(", "):
            directors_name.add(director_name)
        for cast in row.casts.toPython().split(", "):
            casts.add(cast)
        for cast_name in row.castsName.toPython().split(", "):
            casts_name.add(cast_name)
        for country in row.countries.toPython().split(", "):
            countries.add(country)
        for rating in row.ratings.toPython().split(", "):
            ratings.add(rating)
        for genre in row.genres.toPython().split(", "):
            genres.add(genre)

        res_dict['film'] = row.film.toPython()
        res_dict['title'] = row.title.toPython()

        res_dict['directors'] = directors
        res_dict['directors_name'] = directors_name

        res_dict['casts'] = casts
        res_dict['casts_name'] = casts_name

        res_dict['countries'] = countries
        res_dict['ratings'] = ratings
        res_dict['genres'] = genres

        res_dict['date_added'] = row.date_added.toPython()
        res_dict['release_year'] = row.release_year.toPython()
        res_dict['duration'] = row.duration.toPython()
        res_dict['description'] = row.description.toPython()

        res_dicts[res_dict['title']] = res_dict
    
    return res_dicts

def preprocessing(res):
    for _, movie in res.items():
        
        country_dicts = {}
        movie['countries_name'] = []
        for country in movie['countries']:
            if 'https://dbpedia.org/page/' in country:
                country_dicts[country.replace('https://dbpedia.org/page/', '').replace('_', ' ')] = country
                movie['countries_name'].append(country.replace('https://dbpedia.org/page/', '')) 
            else:
                country_dicts[country] = 'https://dbpedia.org/page/' + country.replace(" ", "_")
                movie['countries_name'].append(country)
                
        for country in country_dicts:
            movie['countries'].discard(country)
            movie['countries'].add(country_dicts[country])
            
        genre_dicts = {}
        movie['genres_name'] = []
        for genre in movie['genres']:
            if 'https://dbpedia.org/page/' in genre:
                genre_dicts[genre.replace('https://dbpedia.org/page/', '').replace('_', ' ')] = genre
                movie['genres_name'].append(genre.replace('https://dbpedia.org/page/', '')) 
            else:
                genre_dicts[genre] = 'https://dbpedia.org/page/' + genre.replace(" ", "_")
                movie['genres_name'].append(genre)
                
        for genre in genre_dicts:
            movie['genres'].discard(genre)
            movie['genres'].add(genre_dicts[genre])
   
        movie['duration'] = movie['duration'].replace('-', '')

        if 'Season' not in movie['duration']:
            movie['duration'] = movie['duration'].split(' ')[0] + ' mins'

        cast_list = list(movie['casts'])
        prod_list = list(movie['producers'])
        dir_list = list(movie['directors'])
        writ_list = list(movie['writers'])
        coun_list = list(movie['countries'])
        genre_list = list(movie['genres'])

        movie['casts_zip'] = zip(cast_list, list(map(get_clean_name, cast_list)))
        movie['producers_zip'] = zip(prod_list, list(map(get_clean_name, prod_list)))
        movie['directors_zip'] = zip(dir_list, list(map(get_clean_name, dir_list)))
        movie['writers_zip'] = zip(writ_list, list(map(get_clean_name, writ_list)))
        movie['countries_zip'] = zip(coun_list, list(map(get_clean_name, coun_list)))
        movie['genres_zip'] = zip(genre_list, list(map(get_clean_name, genre_list)))

    return res

def combine(res, local_res):
    for movie in res['results']['bindings']:
        if (movie['title']['value'] in local_res.keys()):
            local_res[movie['title']['value']]['review_score'] = movie.get('review_score', '')
            local_res[movie['title']['value']]['producers'] = set(movie.get('producers', dict()).get('value', '').split(','))
            local_res[movie['title']['value']]['producers_name'] = set(movie.get('producersName', dict()).get('value', '').split(','))
            local_res[movie['title']['value']]['writers'] = set(movie.get('writers', dict()).get('value', '').split(','))
            local_res[movie['title']['value']]['writers_name'] = set(movie.get('writersName', dict()).get('value', '').split(','))
            new_genres = set(movie.get('genres', '').split(', '))
            if new_genres != {''}:
                for genre in new_genres:
                    local_res[movie['title']['value']]['genres'].add(genre)
            new_directors = set(movie.get('directors', dict()).get('value', '').split(','))
            if new_directors != {''}:
                for director in new_directors:
                    local_res[movie['title']['value']]['directors'].add(director)
            new_directors_name = set(movie.get('directorsName', dict()).get('value', '').split(','))
            if new_directors_name != {''}:
                for director in new_directors_name:
                    local_res[movie['title']['value']]['directors_name'].add(director)
            new_casts = set(movie.get('casts', dict()).get('value', '').split(','))
            if new_casts != {''}:
                for cast in new_casts:
                    local_res[movie['title']['value']]['casts'].add(cast)
            new_casts_name = set(movie.get('castsName', dict()).get('value', '').split(','))
            if new_casts_name != {''}:
                for cast in new_casts_name:
                    local_res[movie['title']['value']]['casts_name'].add(cast)
            new_countries = set(movie.get('country').get('value').split(','))
            if new_countries != {''}:
                for country in new_countries:
                    local_res[movie['title']['value']]['countries'].add(country)
            
        else:
            movie_dict = dict()
            movie_dict['title'] = movie['title']['value']

            movie_dict['producers'] = set(movie.get('producers', dict()).get('value', '').split(','))
            movie_dict['producers_name'] = set(movie.get('producersName', dict()).get('value', '').split(','))

            movie_dict['directors'] = set(movie.get('directors', dict()).get('value', '').split(','))
            movie_dict['directors_name'] = set(movie.get('directorsName', dict()).get('value', '').split(','))
            
            movie_dict['writers'] = set(movie.get('writers', dict()).get('value', '').split(','))
            movie_dict['writers_name'] = set(movie.get('writersName', dict()).get('value', '').split(','))

            movie_dict['casts'] = set(movie.get('casts', dict()).get('value', '').split(','))
            movie_dict['casts_name'] = set(movie.get('castsName', dict()).get('value', '').split(','))

            movie_dict['countries'] = set(movie.get('country').get('value').split(','))
            movie_dict['ratings'] = set('')
            movie_dict['review_score'] = movie.get('review_score', '')
            movie_dict['genres'] = set(movie.get('genres', '').split(', '))

            movie_dict['date_added'] = ''
            movie_dict['release_year'] = movie.get('release', dict()).get('value', '')
            movie_dict['duration'] = movie.get('runtime').get('value')
            movie_dict['description'] = movie.get('desc').get('value')

            local_res[movie['title']['value'].lower()] = movie_dict
    for movie in local_res:
        if 'producers' not in local_res[movie].keys():
            local_res[movie]['producers'] = set('')
        if 'producers_name' not in local_res[movie].keys():
            local_res[movie]['producers_name'] = set('')
        if 'writers' not in local_res[movie].keys():
            local_res[movie]['writers'] = set('')
        if 'writers_name' not in local_res[movie].keys():
            local_res[movie]['writers_name'] = set('')
        if 'review_score' not in local_res[movie].keys():
            local_res[movie]['review_score'] = ''
            
        # sanity check
        # for key in local_res[movie].keys():
        #     print(f'{key} : {local_res[movie][key]}')
        # print('\n')

  
    return local_res

def get_clean_name(string):
    tsplit = string.split("/")
    name = tsplit[-1].replace("_", " ")
    return name